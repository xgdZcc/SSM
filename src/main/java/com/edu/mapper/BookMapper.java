package com.edu.mapper;

import com.edu.bean.Book;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Author:max
 * @Date: 2024/11/15 17:00
 * @Description:
 */


public interface BookMapper {

    List<Book> selectAllBooks();

    List<Book> selectBooksByCondition(@Param("book") Book book, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

    int insertBook(Book book);

    int deleteBook(int bookId);

    Book selectBookById(int bookId);

    int updateBook(Book book);

    int updateBookCover(@Param("bookId")int bookId,@Param("bookCover")String bookCover);
}
