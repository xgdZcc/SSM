package com.edu.mapper;

import com.edu.bean.User;
import org.apache.ibatis.annotations.Param;

/**
 * @Author:max
 * @Date: 2024/11/15 16:17
 * @Description:
 */


public interface UserMapper {

    /**
     * 添加用户
     */
    int insertUser(User user);

    /**
     * 根据用户名查找用户
     */
    User selectUserByUsername(String username);

    /**
     * 根据用户名和密码查找用户
     */
    User selectUserByUsernameAndPassword(User user);

    /**
     *
     *  根据用户名来修改用户的头像
     */
    int updateUserHeadImg(@Param("username")String username, @Param("headImg")String headImg);
}
