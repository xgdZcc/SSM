package com.edu.bean;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @Author:max
 * @Date: 2024/11/15 15:39
 * @Description:
 */


public class User {

    private int userId;

    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z\\d]{6,10}$",message = "用户名应该是6-10位的数字和字母的组合")
    private String username;

    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z\\d]{6,10}$",message = "密码应该是6-10位的数字和字母的组合")
    private String password;

    @Range(min = 1,max = 150,message = "年龄应该在1-150之间")
    private int userAge;

    @Past(message = "生日不能大于当前日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Email(message = "邮箱格式不正确")
    private String email;

    private String headImg;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userAge=" + userAge +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", headImg='" + headImg + '\'' +
                '}';
    }

    public User() {
    }
}

