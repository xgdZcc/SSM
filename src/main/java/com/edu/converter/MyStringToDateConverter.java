package com.edu.converter;

import org.springframework.core.convert.converter.Converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyStringToDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        if(source == null || "".equals(source)){
            return null;
        }
        DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        try{
            return dft.parse(source);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
