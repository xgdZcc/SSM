package com.edu.controller;

import com.edu.bean.User;
import com.edu.serivce.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author:max
 * @Date: 2024/11/15 16:13
 * @Description:
 */

@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;// 注入 UserService 服务类的实例，用于处理业务逻辑

    @RequestMapping("/register")
    public String register(MultipartFile avatar,
                           @Valid User user,
                           BindingResult bindingResult,
                           Model model) {
        //输入的后端验证
        if(bindingResult.hasErrors()){
            Map map = new HashMap();
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            for (FieldError fieldError : fieldErrors) {
//                String fieldName = fieldError.getField();
//                String message = fieldError.getDefaultMessage();
//                map.put(fieldName,message);
                map.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
            model.addAttribute("errors", map);
            return "forward:/register.jsp";
        }
        //真正执行注册（不包含把用户的头像添加到数据库）
        int result = userService.userRegister(user);// 调用 UserService 的 userRegister 方法进行用户注册
        if(result == -1){
            model.addAttribute("msg","用户名重复"); // 如果用户名已存在，将错误信息添加到模型中
            return "forward:/register.jsp";// 转发到注册页面，显示错误信息
        }
        // 判断用户上传的文件 存在，且是图片格式时，完成图片上传和用户头像更新
        if(avatar != null && !avatar.isEmpty() && avatar.getContentType().contains("image")){
            try {
                String fileName = avatar.getOriginalFilename();//获取文件原始名字
                String newName = System.currentTimeMillis()+fileName;//通过添加时间戳(或者uuid) 生成一个独一无二的文件名
                String savePath = "c:/headImgs/"+newName; //存储路径，用于文件复制
                String visitPath = "/SSM/headImgs/"+newName;// 访问路径（更新到数据库）记得配置虚拟路径
                avatar.transferTo(new File(savePath));// 把头像 上传到服务器的 指定目录(非项目路径下);
                userService.updateUserImg(user.getUsername(),visitPath);//更新注册成功的用户的头像
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "forward:/login.jsp";// 注册成功，转发到成功页面
    }

    // 处理用户登录请求
    @RequestMapping("/login")
    public String userLogin(User user, Model model, HttpSession session){
        // 调用用户服务类中的用户登录方法，获取登录结果
        Map<String, Object> map = userService.userLogin(user);

        // 如果登录成功
        if(map.get("loginUser")!=null){
            // 将登录用户信息存入session
            session.setAttribute("loginUser",map.get("loginUser"));
            // 重定向到图书页面
            return "redirect:/books";
        }

        // 如果登录失败，将错误信息添加到模型中
        model.addAttribute("msg",map.get("msg"));
        // 重新转发到登录页面
        return "forward:/login.jsp";
    }

}
