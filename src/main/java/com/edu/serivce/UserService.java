package com.edu.serivce;

import com.edu.bean.User;

import java.util.Map;

/**
 * @Author:max
 * @Date: 2024/11/15 16:15
 * @Description:
 */


public interface UserService {
    /**
     * 1成功
     * 0失败
     * -1 用户名已经存在
     * */

    int userRegister(User user);

    /**
     *
     * @param user
     * @return
     *  key中包含 loginUser 登录成功
     *  key中包含 msg 登录失败，并且存放失败的原因
     */
    Map<String,Object> userLogin(User user);

    /**
     * 根据用户名修改用户头像
     * @param username
     * @param headImg
     * @return
     */
    int updateUserImg(String username,String headImg);
}
