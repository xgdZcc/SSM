package com.edu.serivce;

import com.edu.bean.Book;

import java.util.Date;
import java.util.List;

/**
 * @Author:max
 * @Date: 2024/11/15 17:01
 * @Description:
 */


public interface BookService {

    List<Book> selectBooks(Book book, Date beginDate, Date endDate);

    int addBook(Book book);

    int delBook(int bookId);

    Book selectBookById(int bookId);

    int updateBook(Book book);

    int updateBookCover(int bookId,String bookCover);

}
