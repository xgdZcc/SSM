package com.edu.serivce.impl;

import com.edu.bean.Book;
import com.edu.mapper.BookMapper;
import com.edu.serivce.BookService;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author:max
 * @Date: 2024/11/15 17:02
 * @Description:
 */

@Service
public class BookServiceImpl implements BookService {
    
    @Resource
    private BookMapper bookMapper;

    @Override
    public List<Book> selectBooks(Book book, Date beginDate, Date endDate) {
        return bookMapper.selectBooksByCondition(book,beginDate,endDate);
    }

    @Override
    public int addBook(Book book) {
        return bookMapper.insertBook(book);
    }

    @Override
    public int delBook(int bookId) {
        return bookMapper.deleteBook(bookId);
    }

    @Override
    public Book selectBookById(int bookId) {
        return bookMapper.selectBookById(bookId);
    }

    @Override
    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public int updateBookCover(int bookId, String bookCover) {
        return bookMapper.updateBookCover(bookId,bookCover);
    }
}
