package com.edu.serivce.impl;

/**
 * @Author:max
 * @Date: 2024/11/15 16:16
 * @Description:
 */

import com.edu.bean.User;
import com.edu.mapper.UserMapper;
import com.edu.serivce.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    /*
     * 1成功
     * 0失败
     * -1 用户名已经存在
     */

    @Resource
    private UserMapper userMapper;

    @Override
    public int userRegister(User user) {
        if (userMapper.selectUserByUsername(user.getUsername()) != null){
            return -1;
        }
        return userMapper.insertUser(user);
    }

    @Override
    public Map<String, Object> userLogin(User user) {
        final String PASSWORD_ERROR_MSG = "密码错误";
        final String USERNAME_NOT_FOUND_MSG = "用户名不存在";
        User loginUser = userMapper.selectUserByUsernameAndPassword(user);
        Map<String, Object> map = new HashMap<>();
        if (loginUser != null) {
            map.put("loginUser", loginUser);
            return map;
        }
        if (userMapper.selectUserByUsername(user.getUsername()) != null) {
            map.put("msg", PASSWORD_ERROR_MSG);
        } else {
            map.put("msg", USERNAME_NOT_FOUND_MSG);
        }
        return map;
    }

    @Override
    public int updateUserImg(String username, String headImg) {
        return userMapper.updateUserHeadImg(username,headImg);
    }

}
