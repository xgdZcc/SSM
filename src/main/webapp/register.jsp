<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册页面</title>
    <script src="/SSM/js/jquery.js"></script>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div{
            width: 550px;
            height: 330px;
            background: #f9d2e4;
            /*绝对定位*/
            position: absolute;
            /*水平居中*/
            left: 50%;
            /*垂直居中*/
            top: 50%;
            /*移动*/
            transform: translate(-50%,-50%);
            /*圆角*/
            border-radius: 10px;
            /*阴影*/
            box-shadow: 0 0 10px #aaa;
            /*内边距*/
            padding: 20px;
            /*溢出隐藏*/
            overflow: hidden;
            /*水平垂直居中*/
            /*display: flex;*/
            /*垂直居中*/
            align-items: center;
        }
        h2{
            text-align: center;
            margin-bottom: 10px;
        }
        label{
            display: inline-block;
            margin-bottom: 20px;
        }
        form {
            width: 100%;
            padding-top: 10px;
        }
        input{
            line-height: 20px;
        }
        p{
            margin-bottom: 20px;
        }
        #fh{
            font-size: 12px;
            color: red;
            text-decoration: none;
            margin-left: 20px;
        }
    </style>
</head>
<body>
    <div>
        <form action="/SSM/user/register" method="POST" onsubmit="return check()">
            <h2>注册</h2>
            <label>用户名:</label>
            <input type="text" name="username" id="username" placeholder="请输入用户名">
            <span>${msg}</span> <br>

            <label>密码:</label>
            <input type="password" name="password" id="password" placeholder="请输入密码">
            <span>${msg} ${errors.username}</span> <br>

            <label>密码确认:</label>
            <input type="password" name="password1" id="password1" placeholder="请再次输入密码">
            <span></span> <br>

            <label>年龄:</label>
            <input type="number" name="userAge" id="userAge" placeholder="请输入年龄">
            <span>${msg} ${errors.userAge}</span> <br>

            <label>生日:</label>
            <input type="date" name="birthday" id="birthday" placeholder="请输入生日">
            <span>${msg} ${errors.birthday}</span> <br>

            <label>邮箱:</label>
            <input type="email" name="email" id="email" placeholder="请输入邮箱">
            <span>${msg} ${errors.email}</span> <br>

            <label>头像:</label>
            <input type="file" name="avatar" accept="image/*">

            <input id="zc" type="submit" value="注册">
            <a href="login.jsp" id="fh">已有账号，去登录</a>
        </form>
    </div>
</body>
<script>
    function check() {
        let username = $("#username").val();
        let password = $("#password").val();
        let password1 = $("#password1").val();
        let userAge = $("#userAge").val();
        let birthday = $("#birthday").val();
        let email = $("#email").val();
        // 验证是否为空
        let flag1 = true;
        let flag2 = true;
        let flag3 = true;
        let flag4 = true;
        let flag5 = true;
        let flag6 = true;

        //正则表达式
        let reg1 = /^(?!\d+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,10}$/;
        //检查用户名
        if (!reg1.test(username)){
            flag1 = false;
            $("#username").next().text("用户名应该是6-10位的数字和字母组成");
            $("#username").next().addClass("red");
        }else {
            flag1 = true;
            $("#username").next().text("√");
            $("#username").next().addClass("green");
        }

        //检查密码
        if (!reg1.test(password)){
            flag2 = false;
            $("#password").next().text("密码应该是6-10位的数字和字母组成");
            $("#password").next().addClass("red");
        } else {
            flag2 = true;
            $("#password").next().text("√");
            $("#password").next().addClass("green");
        }

        //检查确认密码
        if (password!= password1){
            flag3 = false;
            $("#password1").next().text("两次输入的密码不一致");
            $("#password1").next().addClass("red");
        }else if(password != '' && password1 != ""){
            $("#rePassword").next().text("密码不能为空");
            $("#rePassword").next().addClass("red");
            flag3 = true;
        }else {
            flag3 = true;
            $("#password1").next().text("√");
            $("#password1").next().addClass("green");
        }

        //检查年龄
        if (userAge >= 1 && userAge <= 150){
            flag4 = true;
            $("#userAge").next().text("√");
            $("#userAge").next().addClass("green");
        } else {
            flag4 = false;
            $("#userAge").next().text("年龄应该在1-150之间");
            $("#userAge").next().addClass("red");
        }

        //检查生日
        let now = new Date();
        let birth = new Date(birthday);
        if(birth > now){
            flag5 = false;
            $("#birthday").next().text("生日不能大于当前日期");
            $("#birthday").next().addClass("red");
        }else{
            flag5 = true;
            $("#birthday").next().text("√");
            $("#birthday").next().addClass("green");
        }

        //检查邮箱
        let reg2 = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
        if (!reg2.test(email)){
            flag6 = false;
            $("#email").next().text("邮箱格式不正确");
            $("#email").next().addClass("red");
        } else {
            flag6 = true;
            $("#email").next().text("√");
            $("#email").next().addClass("green");
        }

        return flag1&&flag2&&flag3&&flag4&&flag5&&flag6;
    }
</script>
</html>