<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2024/11/21
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提供登录</title>
    <style>
        *{
            margin: 0px;
            padding: 0px;
        }
        div{
            width: 400px;
            height: 50px;
            margin: auto;
            margin-top: 50px;
            /*文本居中*/
            text-align: center;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            /*圆角*/
            border-radius: 10px;
            /*阴影*/
            box-shadow: 5px 5px 5px #ccc;
            /*文本垂直居中*/
            padding-top: 20px;
            /*文本垂直居中*/
            padding-bottom: 20px;
            /*背景图大小*/
            background-size: 100% 100%;
        }
        a{
            width: 50px;
            height: 35px;
            line-height: 40px;
            /*背景色*/
            background-color: #f5f5f5;
            /*圆角*/
            border-radius: 5px;
            /*左侧外边距*/
            margin-left: 40px;
            /*动画*/
            transition: all 0.5s;
            /*取消下划线*/
            text-decoration: none;
            /*文本颜色*/
            color: black;
        }
        a:hover{
            /*块级元素*/
            display: inline-block;
            /*阴影*/
            box-shadow: 5px 5px 5px #ccc;
            /*鼠标样式*/
            cursor: pointer;
            /*文本颜色*/
            color: #fff;
            /*动画*/
            transition: all 0.5s;
            /*旋转*/
            transform: rotate(360deg);
            /*文本阴影*/
            text-shadow: 0 0 5px #000;
        }
    </style>
</head>
<body>
    <div>
        <h2>
            <a href="login.jsp">登录</a>
            <a href="register.jsp">注册</a>
        </h2>
    </div>
</body>
</html>
