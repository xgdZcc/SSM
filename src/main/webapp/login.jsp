<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div{
            width: 400px;
            height: 230px;
            background-color: #f9d2e4;
            /*绝对定位*/
            position: absolute;
            /*水平居中*/
            left: 50%;
            /*垂直居中*/
            top: 50%;
            /*移动*/
            transform: translate(-50%,-50%);
            /*圆角*/
            border-radius: 10px;
            /*阴影*/
            box-shadow: 0 0 10px #aaa;
            /*内边距*/
            padding: 20px;
            /*溢出隐藏*/
            overflow: hidden;
            /*水平垂直居中*/
            /*display: flex;*/
            /*垂直居中*/
            align-items: center;
        }
        h2{
            text-align: center;
        }
        form {
            width: 100%;
            text-align: center;
            padding-top: 25px;
        }
        input{
            line-height: 20px;
        }
        p{
            margin-bottom: 20px;
            text-align: center;
            color: red;
        }
        #zc{
            font-size: 14px;
            color:black;
            text-decoration: none;
            padding-left: 40px;
        }
    </style>
</head>
<body>
    <div>
        <h2>登录</h2>
        <form action="/SSM/user/login" method="post">
            <label>用户名：</label>
            <input type="text" name="username" placeholder="请输入用户名"> <br><br>
            <label>密码：</label>
            <input type="password" name="password" placeholder="请输入密码"> <br><br>
            <input type="submit" value="登录">
            <a href="/SSM/register.jsp" id="zc">注册</a>
        </form>
        <br>
        <p>${msg}</p>
    </div>
</body>
</html>