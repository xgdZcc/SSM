<%--
  Created by IntelliJ IDEA.
  User: 25654
  Date: 2024/12/7
  Time: 17:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加书籍</title>
    <style>
        *{
            margin: 0px;
            padding: 0px;
        }
        div{
            width: 400px;
            height: 450px;
            margin: 0 auto;
            border: 1px solid #ccc;
            background-color: #f5f5f5;
            margin-top: 50px;
            text-align: center;
            padding-top: 50px;
            border-radius: 10px;
            box-shadow: 0 0 10px #000;
        }
        h2{
            margin-bottom: 30px;
        }
        .ys{
            width: 200px;
            height: 30px;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        input[type=submit]{
            width: 60px;
            height: 40px;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 5px;
            cursor: pointer;
            font-weight: bold;
            font-size: 15px;
        }
        a{
            display: inline-block;
            width: 50px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 20px;
            margin-left: 30px;
            text-decoration: none;
            color: black;
        }
        a:hover{
            background-color: #ccc;
            transition: all 0.5s;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div>
        <h2>添加书籍</h2>
        <form action="/SSM/book/addBook" method="post" enctype="multipart/form-data">
            <label>书名:</label>
            <input type="text" class="ys" name="bookName" required><br>

            <label>作者:</label>
            <input type="text" class="ys" name="bookWriter" required><br>

            <label>类型:</label>
            <input type="text" class="ys" name="bookType" required><br>

            <label>价格:</label>
            <input type="number" class="ys" name="bookPrice" step="0.01" required><br>

            <label>出版日期:</label>
            <input type="date" class="ys" name="pubDate" required><br>

            <label>封面:</label>
            <input type="file" name="mf" required><br>

            <input type="submit" value="添加">
            <a href="/SSM/book.jsp">返回</a>
        </form>
    </div>
</body>
</html>
