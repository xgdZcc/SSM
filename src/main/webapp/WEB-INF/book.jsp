<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> <!-- 引入 JSTL 格式化标签库 -->
<html>
<head>
    <title>图书页面</title>
    <script src="/SSM/js/jquery-1.3.2.min.js"></script>
    <style>
        *{
            margin: 0px;
            padding: 0px;
        }
        table,td{
            border: 1px solid black;
        }
        #chaxun{
            width: 800px;
            height: 60px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            background-color: #f5f5f5;
            margin-top: 50px;
        }
        form{
            width: 100%;
            text-align: center;
        }
        table{
            width: 800px;
            margin: 20px auto;
        }
        tr{
            height: 40px;
            line-height: 40px;
            text-align: center;
        }
        h2{
            text-align: center;
        }
        h3{
            text-align: center;
        }
        a{
            text-decoration: none;
            color: black;
        }
        #pageBar{
            margin-top: 20px;
            text-align: center;
        }
        a:hover{
            cursor: pointer;
        }
        .fy{
            color:blue;
            font-size: 16px;
            font-weight: bold;
            color: #333;
            border: 1px solid #ccc;
            cursor: pointer;
            padding: 5px 10px;
            background-color: #000;
            color: #fff;
            border-radius: 5px;
            margin-right: 10px;
            display: inline-block;
        }
        #pageSize{
            width: 100px;
            margin: 0 auto;
            text-align: center;
            margin-bottom: 10px;
            font-size: 16px;
            font-weight: bold;
            color: #333;
            border: 1px solid #ccc;
            cursor: pointer;
            padding: 5px 10px;
            background-color: #337ab7;
            color: #fff;
            border-radius: 5px;
            display: inline-block;
            margin-right: 10px;
            display: inline-block;
        }
        .headImg{
            width: 50px;
            height: 50px;
        }
        .bookCover{
            width: 80px;
            height: 90px;
        }
        #tj{
            width: 100px;
            height: 30px;
            display: inline-block;
            margin-right: 10px;
            cursor: pointer;
            line-height: 30px;
            text-align: center;
            background-color: #000;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
        }
        #tc{

            /*绝对定位*/
            position: absolute;
            /*水平居中*/
            left: 90%;
            /*垂直居中*/
            top: 2%;

            display: inline-block;
            width: 80px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 20px;
            margin-left: 30px;
            text-decoration: none;
            color: black;

        }
        #tc:hover{
            background-color: #ccc;
            transition: all 0.5s;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <h2>图书管理</h2>
    <p>
        欢迎${sessionScope.loginUser.username}
        <img src="${loginUser.headImg}" alt="" class="headImg">
        <a href="/SSM/index.jsp" id="tc">退出登录</a>
    </p>
    <div id="chaxun">
        <form>
            书名:
            <input type="text" name="bookName" placeholder="书名" id="bookName">
            作者:
            <input type="text" name="bookWriter" placeholder="作者" id="bookWriter">
            类型:
            <input type="text" name="bookType" placeholder="类型" id="bookType"> <br><br>
            起始时间:
            <input type="Date" name="beginDate" id="beginDate">
            结束时间:
            <input type="Date" name="endDate" id="endDate">
            <input type="button" value="查询" id="searchBtn">
        </form>
    </div>
    <a href="/SSM/bookAdd" id="tj">添加书本</a>
    <table>
        <thead>
            <tr>
                <th>图书id</th>
                <th>图书名字</th>
                <th>作者</th>
                <th>图书类型</th>
                <th>图书价格</th>
                <th>入库时间</th>
                <th>封面</th>
                <th>操作1</th>
                <th>操作2</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <!-- 分页控件 -->
    <div id="pageBar">

    </div>
    <!-- 分页数量控制 -->
    <select id="pageSize">
        <option value="5">5</option>
        <option value="10" selected>10</option>
        <option value="15">15</option>
        <option value="20">20</option>
    </select>
</body>

<script>
    let currentPage = 1;

    queryBooks(1);
    // 绑定搜索按钮点击事件
    $("#searchBtn").click(function () {
        queryBooks(1); // 调用queryBooks函数，传入初始页码1
    })

    // 绑定页面大小选择框的change事件
    $("#pageSize").change(function(){
        queryBooks(1); // 页面大小改变时，重新查询第一页数据
    })

    /**
     * 查询书籍数据并更新表格和分页栏
     * @param {number} pageNum - 当前页码
     */
    function queryBooks(pageNum){
        console.log(pageNum); // 输出当前页码到控制台

        // 清空表格内容
        $("tbody").empty();

        // 获取表单中的输入值
        let bookName = $("#bookName").val();
        let bookType = $("#bookType").val();
        let bookWriter = $("#bookWriter").val();
        let beginDate = $("#beginDate").val();
        let endDate = $("#endDate").val();
        let pageSize = $("#pageSize").val();

        // 发送AJAX请求获取书籍数据
        $.ajax({
            url: "/SSM/book/getBooks", // 请求的URL
            type: "get", // 请求方法为GET
            dataType: "json", // 期望服务器返回的数据类型为JSON
            data: {
                "bookName": bookName, // 书籍名称
                "bookType": bookType, // 书籍类型
                "bookWriter": bookWriter, // 作者
                "beginDate": beginDate, // 开始日期
                "endDate": endDate, // 结束日期
                "pageNum": pageNum, // 当前页码
                "pageSize": pageSize // 每页显示数量
            },
            success: function (data) {
                console.log(data); // 输出返回的数据到控制台

                // 提取书籍列表
                let books = data.list;

                // 遍历书籍列表，生成表格行并添加到表格中
                for(let i = 0; i < books.length; i++){
                    let book = books[i];
                    let tr = $("<tr></tr>");
                    tr.append("<td>" + book.bookId + "</td>");
                    tr.append("<td>" + book.bookName + "</td>");
                    tr.append("<td>" + book.bookWriter + "</td>");
                    tr.append("<td>" + book.bookType + "</td>");
                    tr.append("<td>" + book.bookPrice + "</td>");
                    tr.append("<td>" + book.pubDate + "</td>");
                    tr.append(`<td><img src="${"${book.bookCover}"}" class='bookCover'></td>`);
                    tr.append(`<td><a onclick="deleteBook(${"${book.bookId}"})">删除</a></td>`)
                    tr.append(`<td><a href="/SSM/book/update?bookId=${"${book.bookId}"}">修改</a></td>`)
                    $("tbody").append(tr); // 将生成的行添加到表格中
                }

                // 生成分页栏
                makePageBar(data);
            }
        })
    }

    /**
     * 根据返回的数据生成分页栏
     * @param {Object} data - 包含分页信息的数据对象
     */
    function makePageBar(data){
        // 清空分页栏内容
        $("#pageBar").empty();

        // 如果有上一页，生成上一页链接
        if(data.hasPreviousPage){
            let prePage = $(`<a href="#" class="fy" onclick="queryBooks(${"${data.pageNum - 1})"}">上一页</a>`);
            $("#pageBar").append(prePage); // 将上一页链接添加到分页栏
        }

        // 遍历导航页码数组，生成页码链接
        let nums = data.navigatepageNums;
        for(let i = 0; i < nums.length; i++){
            let a = $(`<a href="#" class="fy" onclick="queryBooks(${"${nums[i]}"})">${"${nums[i]}"}</a>`);
            if(data.pageNum == nums[i]){
                a.addClass("red"); // 如果当前页码与遍历的页码相同，添加红色样式
            }

            $("#pageBar").append(a); // 将页码链接添加到分页栏
        }

        // 如果有下一页，生成下一页链接
        if(data.hasNextPage){
            let nextPage = $(`<a href="#" class="fy" onclick="queryBooks(${"${data.pageNum + 1})"}">下一页</a>`);
            $("#pageBar").append(nextPage); // 将下一页链接添加到分页栏
        }
    }
    //删除
    function deleteBook(bookId){
        let confirm = window.confirm("请确认是否要进行删除操作");
        if (confirm == false){
            return ;
        }
        $.ajax({
            url: "/SSM/book/deleteBook",
            type: "post",
            data:{
                "bookId":bookId
            },
            success:function(date) {
                if (date == 'success'){
                    queryBooks(currentPage);
                }else {
                    alert("删除失败")
                }
            }
        })
    }
</script>
</html>
