<%--
  Created by IntelliJ IDEA.
  User: 25654
  Date: 2024/12/14
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div{
            width: 400px;
            height: 500px;
            margin: 0 auto;
            border: 1px solid #ccc;
            background-color: #f5f5f5;
            margin-top: 50px;
            text-align: center;
            padding-top: 50px;
            border-radius: 10px;
            box-shadow: 0 0 10px #000;
        }
        h1{
            margin-bottom: 20px;
        }
        .headImg{
            width: 60px;
            height: 60px;
        }
        .ys{
            width: 200px;
            height: 30px;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        input[type=submit]{
            width: 60px;
            height: 40px;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 5px;
            cursor: pointer;
            font-weight: bold;
            font-size: 15px;
        }
        a{
            display: inline-block;
            width: 50px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            background-color: #f5f5f5;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 20px;
            margin-left: 30px;
            text-decoration: none;
            color: black;
        }
        a:hover{
            background-color: #ccc;
            transition: all 0.5s;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div>
        <h2>修改图书信息</h2><br>
        <form action="/SSM/book/updateBook" method="post" enctype="multipart/form-data">
            <input type="hidden" class="ys" name="bookId" value="${book.bookId}">
            <label>书名:</label>
            <input type="text" class="ys" name="bookName" value="${book.bookName}"> <br>
            <label>作者:</label>
            <input type="text" class="ys" name="bookWriter" value="${book.bookWriter}"> <br>
            <label>类型:</label>
            <input type="text" class="ys" name="bookType" value="${book.bookType}"> <br>
            <label>价格:</label>
            <input type="text" class="ys" name="bookPrice" value="${book.bookPrice}"> <br>
            <label>版时间:</label>出
            <input type="date" name="pubDate" value="<fmt:formatDate value="${book.pubDate}" pattern="yyyy-MM-dd"></fmt:formatDate>"> <br>
            <label>封面:</label>
            <img src="${book.bookCover}" alt="" class="headImg" id="preview"> <br>

            <label>新封面:</label>
            <input type="file" name="mf" id="fileInput"> <br>
            <input type="submit" value="修改"><a href="/SSM/book/book">返回</a>
        </form>
    </div>

</body>
    <script>
        document.getElementById('fileInput').addEventListener('change', function(event) {
            let file = event.target.files[0];
            let reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('preview').src = e.target.result;
            };
            reader.readAsDataURL(file);
        });
    </script>
</html>
