/*
Navicat MySQL Data Transfer

Source Server         : zcc
Source Server Version : 50537
Source Host           : localhost:3306
Source Database       : library

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2024-12-14 18:56:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `bookId` int(11) NOT NULL AUTO_INCREMENT,
  `bookName` varchar(255) NOT NULL,
  `bookWriter` varchar(255) NOT NULL,
  `bookType` varchar(255) NOT NULL,
  `bookPrice` decimal(10,2) NOT NULL,
  `pubDate` date NOT NULL,
  `bookCover` varchar(255) DEFAULT '/SSM/bookCover/default.jpg',
  PRIMARY KEY (`bookId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('2', '百年孤独', '加西亚·马尔克斯', '小说', '39.90', '1967-06-05', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('3', '追风筝的人', '卡勒德·胡赛尼', '小说', '49.90', '2003-05-29', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('4', '平凡的世界', '路遥', '小说', '35.00', '1991-10-01', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('5', '红楼梦', '曹雪芹', '经典', '59.90', '1791-01-01', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('6', '穆斯林的葬礼', '霍达·巴拉卡特', '小说', '45.00', '1990-03-15', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('7', '围城', '钱钟书', '小说', '28.50', '1947-01-01', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('8', '骆驼祥子', '老舍', '小说', '18.00', '1923-11-01', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('9', '阿甘正传', '温斯顿·格鲁姆', '小说', '38.00', '1986-04-23', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('10', '中国哲学简史', '冯友兰', '哲学', '25.00', '1947-08-01', '/SSM/bookCover/default.jpg');
INSERT INTO `book` VALUES ('11', '红楼梦', '曹雪芹', '经典', '59.90', '1791-01-01', '/SSM/bookCover/aa68179d-9d70-41af-b755-8913498bf8ed1.jpg');
INSERT INTO `book` VALUES ('12', 'qqqq', 'aa', 'sa', '23.00', '2024-12-04', '/SSM/bookCover/8d9be2ab-b69f-4728-b43a-68aed2d15e631.jpg');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `userAge` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `headImg` varchar(255) DEFAULT '/SSM/headImgs/default.jpg',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'zhangsan1', 'password123', '25', '1998-05-10', 'zhangsan@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('2', 'lisi22', 'password123', '30', '1993-07-22', 'lisi@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('3', 'wangwu3', 'password123', '28', '1995-12-14', 'wangwu@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('4', 'zhaoliu6', 'password123', '22', '2001-02-28', 'zhaoliu@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('5', 'qianqi5', 'password123', '35', '1988-09-11', 'qianqi@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('14', 'zhoucai1', '123123a', '23', '2024-12-04', '123123@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('17', 'zhoucai2', '123123a', '12', '2024-12-03', '123123@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('18', 'zhoucai3', '123123a', '23', '2024-12-05', '123123@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('19', 'zhoucai4', '123123a', '23', '2024-12-02', '123123@qq.com', null);
INSERT INTO `user` VALUES ('20', 'zhoucai5', '123123a', '23', '2024-12-05', '123123@qq.com', '/SSM/headImgs/default.jpg');
INSERT INTO `user` VALUES ('21', 'zhoucai6', '123123a', '23', '2024-12-11', '123123@qq.com', '/SSM/headImgs/default.jpg');
